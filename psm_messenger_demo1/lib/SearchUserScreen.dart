import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:psm_messenger_demo1/LoginScreen.dart';
import 'package:psm_messenger_demo1/models/Conversation.dart';
import 'package:psm_messenger_demo1/utils/DatabaseHelper.dart';
import 'package:psm_messenger_demo1/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'DialogScreen.dart';
import 'models/Message.dart';
import 'models/User.dart';

class SearchUserScreen extends StatefulWidget {
  @override
  SearchUserScreenState createState() => SearchUserScreenState();
}

class SearchUserScreenState extends State<SearchUserScreen> {
  DatabaseHelper database = DatabaseHelper();
  List<int> activeInterlocutors = new List<int>();
  List<User> usersList = new List<User>();
  List<User> usersToShow = new List<User>();
  Widget _appBarTitle = Text("Search Users");
  bool _showSearchBar = true;
  String currentText = "";

  @override
  void initState() {
    _search();
    _initConversations();
    downloadUsers();
    super.initState();
  }

  void _initConversations() async {
    var conversations = await database.selectConversations();
    for (var c in conversations) {
      activeInterlocutors.add(c.interlocutorID);
    }
  }

  void downloadUsers() async {
    try {
      HttpClientResponse response = await Utils.httpGet("/psm/api/profile/").timeout(Duration(seconds: 5));
      response.transform(utf8.decoder).listen((contents) {
        debugPrint(contents);
        var users = json.decode(contents);
        setState(() {
          for (var u in users) {
            usersList.add(User.fromMap(u));
          }
        });
      });
    } catch (e) {
      debugPrint(e.toString());
      await Utils.showErrorDialog(context, "Error", "Could not fetch the data. Server may be temporary unavailable.");
    }
  }

  void _handleInput(String text) {
    setState(() {
      usersToShow = new List<User>();
      for (var user in usersList) {
        if (!(text == "") && (user.username.toLowerCase()).contains(text) && !activeInterlocutors.contains(user.id)) {
          usersToShow.add(user);
        }
      }
      currentText = text;
    });
  }

  void _search() {
    setState(() {
      _appBarTitle = Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(50),
          ),
        ),
        child: TextFormField(
          autofocus: true,
          style: TextStyle(
            fontSize: 18,
          ),
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(left: 15),
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            hintText: 'Enter a username',
          ),
          //controller: inputController,
          onChanged: (String text) {
            _handleInput(text);
          },
        ),
      );
      _showSearchBar = true;
    });
  }

  void _hide() {
    setState(() {
      _appBarTitle = Text("Search Users");
      _showSearchBar = false;
    });
  }

  void _pushDialogScreen(User user) async {
    final prefs = await SharedPreferences.getInstance();
    final idUser = prefs.getInt('user-id') ?? -1;
    if (idUser == -1) return;
    Conversation dialog = new Conversation(idUser, user.id);
    await database.insertConversation(dialog).then((res) {
      dialog.id = res;
      Navigator.push(context, MaterialPageRoute(builder: (context) => DialogScreen(dialog: dialog)));
    });
  }

  Widget _buildCard(int id, String username, String firstName, String lastName, int p) {
    return Card(
      shape: BeveledRectangleBorder(),
      margin: EdgeInsets.only(top: 1),
      child: InkWell(
        onTap: () {
          _pushDialogScreen(usersToShow[p]);
        },
        child: Container(
          child: Row(
            children: <Widget>[
              Container(
                width: 50,
                height: 50,
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.indigo,
                ),
                child: Center(
                  child: Text(
                    firstName[0] + (lastName.length > 0 ? lastName[0] : ""),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      //fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    firstName + " " + lastName,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.black87,
                    ),
                  ),
                  Padding(padding: const EdgeInsets.fromLTRB(0, 7, 0, 0)),
                  Text(
                    "@" + username,
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              _showSearchBar ? Icons.close : Icons.search,
              color: Colors.white,
            ),
            onPressed: _showSearchBar ? _hide : _search,
          ),
          title: _appBarTitle,
        ),
        body: ListView.builder(
          itemCount: usersToShow.length,
          itemBuilder: (BuildContext context, int p) {
            int id = usersToShow[p].id;
            String username = usersToShow[p].username;
            String firstName = usersToShow[p].firstName;
            String lastName = usersToShow[p].lastName == null ? "" : usersToShow[p].lastName;
            return _buildCard(id, username, firstName, lastName, p);
          },
        )
    );
  }
}