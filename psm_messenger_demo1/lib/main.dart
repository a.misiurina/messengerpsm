import 'dart:io';

import 'package:flutter/material.dart';
import 'package:psm_messenger_demo1/utils/DatabaseHelper.dart';
import 'package:psm_messenger_demo1/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'DialogsList.dart';
import 'LoginScreen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  /*Socket.connect("156.17.235.131", 3001).then((Socket sock) {
    Utils.socket = sock;
    Utils.socket.listen(dataHandler,
        onError: errorHandler,
        onDone: doneHandler,
        cancelOnError: false);
  }).catchError((e) {
    print("Unable to connect: $e");
    exit(1);
  });*/

  Future<bool> isAuth() async {
    final prefs = await SharedPreferences.getInstance();
    final authToken = prefs.getString('auth-token') ?? '';
    final idUser = prefs.getInt('user-id') ?? -1;

    if (authToken != '' && idUser != -1) {
      return true;
    } else {
      return false;
    }
  }

  DatabaseHelper databaseHelper = DatabaseHelper();
  databaseHelper.database;

  Future<bool> auth = isAuth();
  auth.then((a) {
    runApp(MyApp(auth: a));
  }).catchError((e) {
    debugPrint(e.toString());
    runApp(MyApp(auth: false));
  });
}

class MyApp extends StatelessWidget {
  final bool auth;

  const MyApp({Key key, this.auth}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PSM Messenger',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: auth ? DialogsList() : LoginScreen(),
    );
  }
}
