import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:psm_messenger_demo1/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'DialogsList.dart';
import 'EditUserProfileScreen.dart';
import 'LoginScreen.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  RegistrationScreenState createState() => RegistrationScreenState();
}

class RegistrationScreenState extends State<RegistrationScreen> {
  bool showProgress = false;

  String _nameInput = "";
  String _usernameInput = "";
  String _passwordInput = "";
  bool _isUsernameTaken = false;
  String _usernameTakenErrMsg = "";
  bool _isSubmitBtnEnabled = false;
  final c = TextEditingController();
  final _nameInputController = TextEditingController();
  final _usernameInputController = TextEditingController();
  final _passwordInputController = TextEditingController();

  @override
  void dispose() {
    _nameInputController.dispose();
    _usernameInputController.dispose();
    _passwordInputController.dispose();
    super.dispose();
  }

  // TODO
  _filterNameInput(String text) {
    // validation

    setState(() {
      _nameInput = text;
    });
  }

  // TODO
  _filterUsernameInput(String text) async {
    // validation

    HttpClientResponse response = await Utils.httpGet('/psm/api/usernames/$text').timeout(Duration(seconds: 1));
    response.transform(utf8.decoder).listen((contents) {
      debugPrint(contents);
      if (contents == "true") {
        _isUsernameTaken = true;
        _usernameTakenErrMsg = "Username @${_usernameInput.toLowerCase()} is already taken.";
      } else {
        _isUsernameTaken = false;
      }
    });

    setState(() {
      _usernameInput = text;
    });
  }

  // TODO
  Future<void> _postRegistrationForm() async {
    List<String> names = _nameInput.split(' ');
    String firstName = '';
    String lastName = '';
    if (names.length > 1) {
      for (int i = 0; i < names.length - 1; i++) {
        firstName += names[i] + (i < names.length - 2 ? ' ' : '');
      }
      lastName = names[names.length - 1];
    } else {
      firstName = names[0];
    }

    Map jsonData = {
      'first_name': firstName,
      'last_name': lastName == "" ? null : lastName,
      'username': _usernameInput,
      'password': _passwordInput
    };

    setState(() {
      showProgress = true;
    });

    try {
      HttpClientResponse response = await Utils.httpPost("/psm/api/signup", jsonData).timeout(Duration(seconds: 5));
      response.transform(utf8.decoder).listen((contents) async {
        if (response.statusCode == 200) {
          Map vm = json.decode(contents);
          //await Utils.showErrorDialog(context, 'IDUser: $contents', 'auth-token: ${response.headers.value('x-auth-token')}');
          final prefs = await SharedPreferences.getInstance();
          prefs.setString('auth-token', response.headers.value('x-auth-token'));
          prefs.setInt('user-id', vm['id']/*int.parse(contents)*/);
          Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (context) => EditUserProfileScreen()),
          );
        } else if (response.statusCode == 400) {
          //await Utils.showErrorDialog(context, "Registration failed", "Server responded with the status code ${response.statusCode}:\n$contents");
          Scaffold.of(context).showSnackBar(SnackBar(content: Text(contents)));
        } else {
          await Utils.showErrorDialog(context, "Registration failed", "Server responded with the status code ${response.statusCode}:\n$contents");
        }
      });
    } catch (e) {
      await Utils.showErrorDialog(context, "Registration failed", "Server may be temporary unavailable.");
    }

    setState(() {
      showProgress = false;
    });
  }

  Widget buildRegistrationForm() {
    return showProgress
        ? Center(
          child: CircularProgressIndicator(),
        )
        : ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context, int position) {
          return Container(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                  child: TextFormField(
                    autofocus: true,
                    textCapitalization: TextCapitalization.words,
                    autocorrect: false,
                    cursorColor: Colors.indigo,
                    cursorRadius: Radius.circular(1),
                    textInputAction: TextInputAction.next,
                    controller: _nameInputController,
                    onChanged: (String text) {
                      _filterNameInput(text);
                    },
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).nextFocus();
                    },
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      labelText: "Name",
                      border: OutlineInputBorder(),
                      contentPadding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                  child: TextFormField(
                    autocorrect: false,
                    cursorColor: Colors.indigo,
                    cursorRadius: Radius.circular(1),
                    textInputAction: TextInputAction.next,
                    controller: _usernameInputController,
                    onChanged: (String text) {
                      _filterUsernameInput(text);
                    },
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).nextFocus();
                    },
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      prefixText: "@",
                      labelText: "Username",
                      border: OutlineInputBorder(),
                      contentPadding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                      errorMaxLines: 3,
                      errorText: _isUsernameTaken ? _usernameTakenErrMsg : null,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                  child: TextFormField(
                      obscureText: true,
                      autocorrect: false,
                      cursorColor: Colors.indigo,
                      cursorRadius: Radius.circular(1),
                      textInputAction: TextInputAction.done,
                      controller: _passwordInputController,
                      onChanged: (String text) {
                        setState(() {
                          _passwordInput = text;
                        });
                      },
                      onFieldSubmitted: (v) {
                        _postRegistrationForm();
                      },
                      style: TextStyle(
                        fontSize: 18,
                      ),
                      decoration: InputDecoration(
                        labelText: "Password",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                        errorMaxLines: 3,
                        //errorText: _validatePassword() ? null : _pasValErrMsg,
                      )),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 5),
                          child: FlatButton(
                            textColor: Colors.indigo,
                            onPressed: () {
                              Navigator.pushReplacement(context, MaterialPageRoute(
                                  builder: (context) => LoginScreen()),
                              );
                            },
                            child: Text("Sign In"),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 5),
                          child: RaisedButton(
                            color: Colors.indigo,
                            textColor: Colors.white,
                            disabledColor: Colors.grey,
                            disabledTextColor: Colors.black,

                            onPressed: !_isSubmitBtnEnabled ? null : () {
                              _postRegistrationForm();
                            },
                            child: Text("Sign Up"),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    if (_nameInputController.text.length > 0
        && _usernameInputController.text.length > 0
        && _passwordInputController.text.length >= 8
        && !_isUsernameTaken) {
      _isSubmitBtnEnabled = true;
    } else {
      _isSubmitBtnEnabled = false;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Sign up"),
      ),
      body: buildRegistrationForm(),
    );
  }
}
