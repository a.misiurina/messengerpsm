import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:psm_messenger_demo1/models/Conversation.dart';
import 'package:psm_messenger_demo1/utils/DatabaseHelper.dart';
import 'package:bubble/bubble.dart';
import 'package:psm_messenger_demo1/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'models/Message.dart';
import 'models/User.dart';

class DialogScreen extends StatefulWidget {
  final Conversation dialog;

  const DialogScreen({Key key, this.dialog}) : super(key: key);

  @override
  DialogScreenState createState() => DialogScreenState();
}

class DialogScreenState extends State<DialogScreen> {
  DatabaseHelper database = DatabaseHelper();
  List<Message> messagesList = new List<Message>();
  User interlocutor;
  User currentUser;
  String currentMessage;
  final inputController = TextEditingController();

  @override
  void initState() {
    _setUsers(); // SOZDAST OSHIBKU BO NE AWAIT?
    super.initState();
  }

  @override
  void dispose() {
    inputController.dispose();
    super.dispose();
  }

  void _setUsers() async {
    try {
      HttpClientResponse response = await Utils.httpGet("/psm/api/profile/${widget.dialog.interlocutorID}").timeout(Duration(seconds: 5));
      response.transform(utf8.decoder).listen((contents) {
        debugPrint(contents);
        var user = json.decode(contents);
        setState(() {
          interlocutor = User.fromMap(user);
        });
      });

      HttpClientResponse response1 = await Utils.httpGet("/psm/api/profile/me").timeout(Duration(seconds: 5));
      response1.transform(utf8.decoder).listen((contents) {
        debugPrint(contents);
        var user = json.decode(contents);
        setState(() {
          currentUser = User.fromMap(user);
        });
      });
    } catch (e) {
      debugPrint(e.toString());
      await Utils.showErrorDialog(context, "Error", e.toString());
    }
  }

  void _updateMessages() {
    database.selectMessages(widget.dialog).then((list) {
      setState(() {
        this.messagesList = list;
        // debugPrint("Hello" + list.length.toString());
      });
    });
  }

  void _sendMessage() {
    debugPrint(currentMessage);
    debugPrint(widget.dialog.id.toString());
    if (currentMessage.length > 0) {
      var msg = new Message(
        widget.dialog.id,
        widget.dialog.userID,
        widget.dialog.interlocutorID,
        currentMessage,
      );
      database.insertMessage(msg);
      inputController.clear();
      _updateMessages();
    }
  }

  Bubble _buildListItem(Message message) {
    bool incoming = (message.senderID == widget.dialog.interlocutorID && message.senderID != currentUser.id);
    double px = 1 / MediaQuery.of(context).devicePixelRatio;
    return Bubble(
      margin: BubbleEdges.only(bottom: 5),
      padding: BubbleEdges.all(7),
      //padding: BubbleEdges.fromLTRB(10, 7, 27, 7),
      radius: Radius.circular(20.0),
      elevation: 3 * px,
      alignment: incoming ? Alignment.centerLeft : Alignment.centerRight,
      color: incoming ? Colors.white : Colors.indigo,
      child: Text(message.text, style: TextStyle(
        color: incoming ? Colors.black : Colors.white,
        fontSize: 16,
      ),
      ),
    );
  }

  Widget _buildMessagesList() {
    return messagesList.length == 0
    ? Center(
      child: Bubble(
        margin: BubbleEdges.only(top: 10),
        radius: Radius.circular(20.0),
        alignment: Alignment.center,
        color: Color.fromRGBO(0, 0, 0, .3),
        child: Text('No messages yet',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 11.0, color: Colors.white),
        ),
      ),
    )
    : ListView.builder(
      reverse: true,
      itemCount: messagesList.length,
      itemBuilder: (BuildContext context, int position) {
        return _buildListItem(messagesList[position]);
      },
    );
  }

  Expanded _buildDialogContainer() {
    return Expanded(
      child: Container(
        //padding: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/chat_background.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: _buildMessagesList(),
      ),
    );
  }

  Container _buildTextInput() {
    return Container(
      width: MediaQuery.of(context).size.width - 55,
      child: TextFormField(
        autofocus: true,
        textCapitalization: TextCapitalization.sentences,
        style: TextStyle(
          fontSize: 18,
        ),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(left: 15),
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          hintText: 'Start typing your message...',
        ),
        maxLines: 5,
        minLines: 1,
        controller: inputController,
        onChanged: (String text) {
          setState(() {
            currentMessage = text;
          });
        },
      ),
    );
  }

  InkWell _buildSendButton() {
    return InkWell(
      onTap: _sendMessage,
      child: Container(
        margin: EdgeInsets.only(left: 10, bottom: 7, right: 10),
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.transparent,
        ),
        child: Icon(
          Icons.send,
          color: Colors.indigo,
          size: 25,
        ),
      ),
    );
  }

  Container _buildMessageInputBar() {
    return Container(
      // text field
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          _buildTextInput(),
          _buildSendButton(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (messagesList == null) {
      messagesList = new List<Message>();
    }
    _updateMessages();

    return Scaffold(
      appBar: AppBar(
        title: Text(interlocutor != null ?
        interlocutor.firstName + " " + interlocutor.lastName : ""),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          _buildDialogContainer(),
          _buildMessageInputBar(),
        ],
      ),
    );
  }
}
