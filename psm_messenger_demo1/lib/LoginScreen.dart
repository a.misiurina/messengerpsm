import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:psm_messenger_demo1/RegistrationScreen.dart';
import 'package:psm_messenger_demo1/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'DialogsList.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  bool showProgress = false;

  String _usernameInput = "";
  String _passwordInput = "";
  bool _isSubmitBtnEnabled = false;
  final _usernameInputController = TextEditingController();
  final _passwordInputController = TextEditingController();

  @override
  void dispose() {
    _usernameInputController.dispose();
    _passwordInputController.dispose();
    super.dispose();
  }

  // TODO
  _filterUsernameInput(String text) {
    // validation

    setState(() {
      _usernameInput = text;
    });
  }

  // TODO
  Future<void> _postLoginForm() async {
    Map jsonData = {
      'username': _usernameInput,
      'password': _passwordInput
    };

    setState(() {
      showProgress = true;
    });

    try {
      HttpClientResponse response = await Utils.httpPost("/psm/api/login", jsonData).timeout(Duration(seconds: 5));
      response.transform(utf8.decoder).listen((contents) async {
        if (response.statusCode == 200) {
          Map vm = json.decode(contents);
          debugPrint(vm['id'].runtimeType.toString());
          //await Utils.showErrorDialog(context, 'IDUser: $contents', 'auth-token: ${response.headers.value('x-auth-token')}');
          final prefs = await SharedPreferences.getInstance();
          prefs.setString('auth-token', response.headers.value('x-auth-token'));
          prefs.setInt('user-id', vm['id']);
          Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (context) => DialogsList()),
          );
        } else if (response.statusCode == 400) {
          Scaffold.of(context).showSnackBar(SnackBar(content: Text(contents)));
          //await Utils.showErrorDialog(context, "Sign in failed", "Server responded with the status code ${response.statusCode}:\n$contents");
        } else {
          await Utils.showErrorDialog(context, "Sign in failed", "Server responded with the status code ${response.statusCode}:\n$contents");
        }
      });
    } catch (e) {
      await Utils.showErrorDialog(context, "Sign in failed", "Server may be temporary unavailable.");
    }

    setState(() {
      showProgress = false;
    });
  }

  Widget buildLoginForm() {
    return showProgress
      ? Center(
        child: CircularProgressIndicator(),
      )
      : ListView.builder(
      itemCount: 1,
      itemBuilder: (BuildContext context, int position) {
        return Container(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                child: TextFormField(
                  autofocus: true,
                  autocorrect: false,
                  cursorColor: Colors.indigo,
                  cursorRadius: Radius.circular(1),
                  textInputAction: TextInputAction.next,
                  controller: _usernameInputController,
                  onChanged: (String text) {
                    _filterUsernameInput(text);
                  },
                  onFieldSubmitted: (v) {
                    FocusScope.of(context).nextFocus();
                  },
                  style: TextStyle(
                    fontSize: 18,
                  ),
                  decoration: InputDecoration(
                    prefixText: "@",
                    labelText: "Username",
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                child: TextFormField(
                  obscureText: true,
                  autocorrect: false,
                  cursorColor: Colors.indigo,
                  cursorRadius: Radius.circular(1),
                  textInputAction: TextInputAction.done,
                  controller: _passwordInputController,
                  onChanged: (String text) {
                    setState(() {
                      _passwordInput = text;
                    });
                  },
                  onFieldSubmitted: (v) {
                    _postLoginForm();
                  },
                  style: TextStyle(
                    fontSize: 18,
                  ),
                  decoration: InputDecoration(
                    labelText: "Password",
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(right: 5),
                        child: FlatButton(
                          textColor: Colors.indigo,
                          onPressed: () {
                            Navigator.pushReplacement(context, MaterialPageRoute(
                                builder: (context) => RegistrationScreen()),
                            );
                          },
                          child: Text("Sign Up"),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 5),
                        child: RaisedButton(
                          color: Colors.indigo,
                          textColor: Colors.white,
                          disabledColor: Colors.grey,
                          disabledTextColor: Colors.black,
                          onPressed: !_isSubmitBtnEnabled ? null : () {
                            _postLoginForm();
                          },
                          child: Text("Sign In"),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      });
  }

  @override
  Widget build(BuildContext context) {
    if (_usernameInputController.text.length > 0
        && _passwordInputController.text.length >= 8) {
      _isSubmitBtnEnabled = true;
    } else {
      _isSubmitBtnEnabled = false;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Sign in"),
      ),
      body: buildLoginForm(),
    );
  }
}
