import 'package:flutter/widgets.dart';

class PSMMessengerIcons {
  PSMMessengerIcons._();

  static const _kFontFam = 'PSMMessengerIcons';
  static const _kFontPkg = null;

  static const IconData send = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
