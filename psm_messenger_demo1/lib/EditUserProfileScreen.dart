import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:psm_messenger_demo1/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'DialogsList.dart';
import 'LoginScreen.dart';

class EditUserProfileScreen extends StatefulWidget {
  @override
  EditUserProfileScreenScreenState createState() => EditUserProfileScreenScreenState();
}

class EditUserProfileScreenScreenState extends State<EditUserProfileScreen> {
  String _username = "";
  String _firstNameInput = "";
  String _lastNameInput = "";
  String _emailInput = "";
  String _phoneNumberInput = "";
  String _aboutInput = "";
  bool _isSubmitBtnEnabled = true;
  final _firstNameInputController = TextEditingController();
  final _lastNameInputController = TextEditingController();
  final _emailInputController = TextEditingController();
  final _phoneNumberInputController = TextEditingController();
  final _aboutInputController = TextEditingController();

  @override
  void initState() {
    onLoad();
    super.initState();
  }

  @override
  void dispose() {
    _firstNameInputController.dispose();
    _lastNameInputController.dispose();
    _emailInputController.dispose();
    _phoneNumberInputController.dispose();
    _aboutInputController.dispose();
    super.dispose();
  }

  // TODO
  _filterFirstNameInput(String text) {
    // validation

    setState(() {
      _firstNameInput = text;
    });
    if (_firstNameInputController.text.length > 0) {
      _isSubmitBtnEnabled = true;
    } else {
      _isSubmitBtnEnabled = false;
    }
  }

  // TODO
  _filterLastNameInput(String text) {
    // validation

    setState(() {
      _lastNameInput = text;
    });
  }

  // TODO
  _filterEmailInput(String text) {
    // validation

    setState(() {
      _emailInput = text;
    });
  }

  // TODO
  _filterPhoneNumberInput(String text) {
    // validation

    setState(() {
      _phoneNumberInput = text;
    });
  }

  // TODO
  _filterAboutInput(String text) {
    // validation

    setState(() {
      _aboutInput = text;
    });
  }

  Future<void> onLoad() async {
    try {
      HttpClientResponse response = await Utils.httpGet("/psm/api/profile/me").timeout(Duration(seconds: 1));
      response.transform(utf8.decoder).listen((contents) {
        debugPrint(contents);
        Map vm = json.decode(contents);
        _username = vm['username'] != null ? vm['username'].toString() : "";
        _firstNameInput = vm['first_name'] != null ? vm['first_name'].toString() : "";
        _lastNameInput = vm['last_name'] != null ? vm['last_name'].toString() : "";
        _emailInput = vm['email'] != null ? vm['email'].toString() : "";
        _phoneNumberInput = vm['phone_number'] != null ? vm['phone_number'].toString() : "";
        _aboutInput = vm['about'] != null ? vm['about'].toString() : "";
        _firstNameInputController.text = _firstNameInput;
        _lastNameInputController.text = _lastNameInput;
        _emailInputController.text = _emailInput;
        _phoneNumberInputController.text = _phoneNumberInput;
        _aboutInputController.text = _aboutInput;
      });
    } catch (e) {
      await Utils.showErrorDialog(context, "Error", "Could not fetch the data. Server may be temporary unavailable.");
    }
  }

  // TODO
  void _postUserProfileForm() async {
    Map jsonData = {
      'username': _username,
      'first_name': _firstNameInput,
      'last_name': _lastNameInput == "" ? null : _lastNameInput,
      'email': _emailInput,
      'phone_number': _phoneNumberInput,
      'about': _aboutInput
    };

    try {
      HttpClientResponse response = await Utils.httpPut("/psm/api/profile/me", jsonData).timeout(Duration(seconds: 5));
      response.transform(utf8.decoder).listen((contents) async {
        if (response.statusCode == 200) {
          Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context) => DialogsList()),
          );
        } else if (response.statusCode == 400) {
          Scaffold.of(context).showSnackBar(SnackBar(content: Text(contents)));
        } else {
          await Utils.showErrorDialog(context, "Registration failed", "Server responded with the status code ${response.statusCode}:\n$contents");
        }
      });
    } catch (e) {
      await Utils.showErrorDialog(context, "Registration failed", "Server may be temporary unavailable.");
    }
  }

  Widget buildRegistrationForm() {
    return ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context, int position) {
          return Container(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                  child: Text("You can provide additional info about yourself. These fields are not requied.",
                    style: TextStyle(
                      fontSize: 14,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                  child: TextFormField(
                    textCapitalization: TextCapitalization.words,
                    autocorrect: false,
                    cursorColor: Colors.indigo,
                    cursorRadius: Radius.circular(1),
                    textInputAction: TextInputAction.next,
                    controller: _firstNameInputController,
                    onChanged: (String text) {
                      _filterFirstNameInput(text);
                    },
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).nextFocus();
                    },
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      labelText: "Name",
                      border: OutlineInputBorder(),
                      contentPadding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                  child: TextFormField(
                    textCapitalization: TextCapitalization.words,
                    autocorrect: false,
                    cursorColor: Colors.indigo,
                    cursorRadius: Radius.circular(1),
                    textInputAction: TextInputAction.next,
                    controller: _lastNameInputController,
                    onChanged: (String text) {
                      _filterLastNameInput(text);
                    },
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).nextFocus();
                    },
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      labelText: "Last Name",
                      border: OutlineInputBorder(),
                      contentPadding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                  child: TextFormField(
                    autocorrect: false,
                    cursorColor: Colors.indigo,
                    cursorRadius: Radius.circular(1),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.emailAddress,
                    controller: _emailInputController,
                    onChanged: (String text) {
                      _filterEmailInput(text);
                    },
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).nextFocus();
                    },
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      labelText: "Email",
                      border: OutlineInputBorder(),
                      contentPadding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                  child: TextFormField(
                    autocorrect: false,
                    cursorColor: Colors.indigo,
                    cursorRadius: Radius.circular(1),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.phone,
                    controller: _phoneNumberInputController,
                    onChanged: (String text) {
                      _filterPhoneNumberInput(text);
                    },
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).nextFocus();
                    },
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      prefixText: "+48 ",
                      labelText: "Phone Number",
                      border: OutlineInputBorder(),
                      contentPadding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                  child: TextFormField(
                    cursorColor: Colors.indigo,
                    cursorRadius: Radius.circular(1),
                    keyboardType: TextInputType.multiline,
                    minLines: 5,
                    maxLines: 5,
                    controller: _aboutInputController,
                    onChanged: (String text) {
                      _filterAboutInput(text);
                    },
                    onFieldSubmitted: (v) {
                      _postUserProfileForm();
                    },
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      labelText: "About",
                      alignLabelWithHint: true,
                      border: OutlineInputBorder(),
                      contentPadding: EdgeInsets.fromLTRB(15, 25, 15, 0),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 5),
                        child: RaisedButton(
                          color: Colors.indigo,
                          textColor: Colors.white,
                          disabledColor: Colors.grey,
                          disabledTextColor: Colors.black,
                          padding: EdgeInsets.fromLTRB(40, 0, 40, 0),

                          onPressed: !_isSubmitBtnEnabled ? null : () {
                            _postUserProfileForm();
                          },
                          child: Text("Finish"),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Additional info"),
      ),
      body: buildRegistrationForm(),
    );
  }
}
