import 'dart:io';
import 'dart:async';
import 'package:psm_messenger_demo1/models/Conversation.dart';
import 'package:psm_messenger_demo1/models/Message.dart';
import 'package:psm_messenger_demo1/models/User.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'dart:developer';

class DatabaseHelper {
  static DatabaseHelper _databaseHelper;
  static Database _database;
  List<String> _databaseCreateSeq = [
    '''
    CREATE TABLE IF NOT EXISTS DialogsList (
      IDDialog             INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
      IDUser               integer(10) NOT NULL, 
      IDInterlocutor       integer(10) NOT NULL, 
      NotificationsEnabled integer(1) DEFAULT 0
    )
    ''',
    '''
    CREATE TABLE IF NOT EXISTS Message (
      IDMessage       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
      IDDialog        integer(10) NOT NULL, 
      IDSender        integer(10) NOT NULL, 
      IDRecipient     integer(10) NOT NULL, 
      SendingTime     integer(11) NOT NULL, 
      MessageContents text NOT NULL, 
      EncryptionKey   text NOT NULL, 
      IsSeen          integer(1) DEFAULT 0, 
      FOREIGN KEY(IDDialog) REFERENCES DialogsList(IDDialog)
    )
    ''',
    'CREATE UNIQUE INDEX DialogsList_IDDialog ON DialogsList (IDDialog)',
    'CREATE UNIQUE INDEX Message_IDMessage ON Message (IDMessage)'
  ];

  DatabaseHelper._createInstance();

  factory DatabaseHelper() {
    if (_databaseHelper == null)
      _databaseHelper = DatabaseHelper._createInstance();
    return _databaseHelper;
  }

  Future<Database> initDatabase() async {
    String path = join(await getDatabasesPath(), "psmmessenger.db");
    return await openDatabase(path, version: 1, onCreate: _createDatabase);
  }

  void _createDatabase(Database db, int newVersion) async {
    for (var q in _databaseCreateSeq) {
      await db.execute(q);
    }
  }

  Future<Database> get database async {
    if (_database == null) _database = await initDatabase();
    return _database;
  }

  /*Future<User> selectUser(int id) async {
    Database db = await this.database;
    List<Map<String, dynamic>> mapList = await db.query("User", where: "IDUser = $id");
    List<User> users = List<User>();
    for (Map m in mapList) {
      users.add(User.fromMap(m));
    }
    return mapList.length > 0 ? users[0] : null;
  }*/

  Future<int> insertConversation(Conversation conversation) async {
    Database db = await this.database;
    return await db.insert("DialogsList", conversation.toMap());
  }

  Future<List<Conversation>> selectConversations() async {
    Database db = await this.database;
    List<Map<String, dynamic>> mapList = await db.query("DialogsList");
    List<Conversation> conversations = List<Conversation>();
    for (Map m in mapList) {
      conversations.add(Conversation.fromMap(m));
    }
    return conversations;
  }

  Future<int> insertMessage(Message message) async {
    Database db = await this.database;
    return await db.insert("Message", message.toMap());
  }

  Future<List<Message>> selectMessages(Conversation c) async {
    Database db = await this.database;
    List<Map<String, dynamic>> mapList = await db.query("Message", where: "IDDialog = ${c.id}", orderBy: "SendingTime DESC");
    List<Message> messages = List<Message>();
    for (Map m in mapList) {
      messages.add(Message.fromMap(m));
    }
    return messages;
  }

  Future<Message> selectLastMessage(Conversation c) async {
    Database db = await this.database;
    List<Map<String, dynamic>> mapList = await db.query("Message", where: "IDDialog = ${c.id}", orderBy: "SendingTime DESC", limit: 1);
    List<Message> messages = List<Message>();
    for (Map m in mapList) {
      messages.add(Message.fromMap(m));
    }
    return mapList.length > 0 ? messages[0] : null;
  }

  /*Future<List<User>> getUsersFromDB() async {
    Database db = await this.database;
    List<Map<String, dynamic>> mapList = await db.query("User");
    List<User> users = List<User>();
    for (Map m in mapList) {
      users.add(User.fromMap(m));
    }
    return users;
  }

  Future<List<Message>> getMessagesFromDB(User sender) async {
    Database db = await this.database;
    List<Map<String, dynamic>> mapList = await db.query("Message",
        //where: "IDSender = ${sender.id}",
        orderBy: "SendingTime DESC");
    List<Message> messages = List<Message>();
    for (Map m in mapList) {
      messages.add(Message.fromMap(m));
    }
    return messages;
  }

  Future<int> addUserToDB(User user) async {
    Database db = await this.database;
    var result = await db.insert("User", user.toMap());
    return result;
  }

  Future<int> addMessageToDB(Message message) async {
    Database db = await this.database;
    return await db.insert("Message", message.toMap());
  }*/
}
