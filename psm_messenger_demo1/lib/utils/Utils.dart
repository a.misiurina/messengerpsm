import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Utils {
  //static Socket socket;

  static Future<void> showErrorDialog(context, String title, String text) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(text),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  String encryptDecrypt(String input, String key) {
    var charKey = [];
    for (int i = 0; i < key.length; i++) {
      charKey.add(key[i]);
    }
    var output = [];

    for(var i = 0; i < input.length; i++) {
      var charCode = input.codeUnitAt(i) ^ key[i % key.length].codeUnitAt(0);
      output.add(new String.fromCharCode(charCode));
    }

    return output.join("");
  }

  static String _host = "192.168.1.246";
  static int _port = 3001;

  static Future<HttpClientResponse> httpPost(String route, Map jsonData) async {
    final prefs = await SharedPreferences.getInstance();
    final authToken = prefs.getString('auth-token') ?? '';

    HttpClientRequest request = await HttpClient().post(_host, _port, route)
      ..headers.contentType = ContentType.json
      ..headers.add('x-auth-token', authToken)
      ..write(jsonEncode(jsonData));
    return await request.close();
  }

  static Future<HttpClientResponse> httpPut(String route, Map jsonData) async {
    final prefs = await SharedPreferences.getInstance();
    final authToken = prefs.getString('auth-token') ?? '';

    HttpClientRequest request = await HttpClient().put(_host, _port, route)
      ..headers.contentType = ContentType.json
      ..headers.add('x-auth-token', authToken)
      ..write(jsonEncode(jsonData));
    return await request.close();
  }

  static Future<HttpClientResponse> httpGet(String route) async {
    final prefs = await SharedPreferences.getInstance();
    final authToken = prefs.getString('auth-token') ?? '';

    HttpClientRequest request = await HttpClient().get(_host, _port, route)
      ..headers.contentType = ContentType.json
      ..headers.add('x-auth-token', authToken);
    return await request.close();
  }
}