import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:psm_messenger_demo1/LoginScreen.dart';
import 'package:psm_messenger_demo1/utils/DatabaseHelper.dart';
import 'package:psm_messenger_demo1/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'DialogScreen.dart';
import 'SearchUserScreen.dart';
import 'models/Message.dart';
import 'models/User.dart';
import 'models/Conversation.dart';

class DialogsList extends StatefulWidget {
  @override
  DialogsListState createState() => DialogsListState();
}

class DialogsListState extends State<DialogsList> {
  DatabaseHelper database = DatabaseHelper();
  List<Conversation> conversations = new List<Conversation>();
  Map<Conversation, User> users = new Map<Conversation, User>();
  Map<Conversation, Message> lastMessages = new Map<Conversation, Message>();

  @override
  void initState() {
    _initConversations();
    super.initState();
  }

  void _initConversations() async {
    try {
      var cs = await database.selectConversations();
      setState(() async {
        conversations = cs;
      });
      for (var c in cs) {
        HttpClientResponse response = await Utils.httpGet("/psm/api/profile/${c.interlocutorID}").timeout(Duration(seconds: 1));
        response.transform(utf8.decoder).listen((contents) async {
          Map vm = json.decode(contents);
          User interlocutor = User.fromMap(vm);
          Message message = await database.selectLastMessage(c);
          setState(() {
            users[c] = interlocutor;
            lastMessages[c] = message;
          });
        });
      }
    } catch (e) {
      debugPrint(e.toString());
      await Utils.showErrorDialog(context, "Error", "Could not fetch the data. Server may be temporary unavailable.");
    }
  }

  Future<void> logOut() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('auth-token');
    prefs.remove('user-id');

    Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context) => LoginScreen()),
    );
  }

  void _pushDialogScreen(Conversation dialog) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => DialogScreen(dialog: dialog))).then((value) {
          return _initConversations();
        });
  }

  Widget _buildCard(Conversation dialog) {
    /*database.getMessagesFromDB(user).then((list) {
      setState(() {
        this.userMessages[user] = list;
      });
    });*/

    String lastMessage = "No messages yet";
    if (lastMessages[dialog] != null) {
      lastMessage = lastMessages[dialog].recipientID == dialog.interlocutorID ? "You: " : "";
      lastMessage += lastMessages[dialog].text;
    }

    return Card(
      shape: BeveledRectangleBorder(),
      margin: EdgeInsets.only(top: 1),
      child: InkWell(
        onTap: () {
          _pushDialogScreen(dialog);
        },
        child: Container(
          child: Row(
            children: <Widget>[
              Container(
                width: 50,
                height: 50,
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.indigo,
                ),
                child: Center(
                  child: Text(
                    users[dialog].firstName[0] + (users[dialog].lastName.length > 0 ? users[dialog].lastName[0] : ""),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      //fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    users[dialog].firstName + " " + users[dialog].lastName,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.black87,
                    ),
                  ),
                  Padding(padding: const EdgeInsets.fromLTRB(0, 7, 0, 0)),
                  Text(
                    lastMessage,
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dialogs"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.power_settings_new,
              color: Colors.white,
            ),
            onPressed: logOut,
          ),
        ],
      ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () async {
            Navigator.push(context, MaterialPageRoute(builder: (context) => SearchUserScreen())).then((value) {
              return _initConversations();
            });
          },
          label: Text('Add chat'),
          icon: Icon(Icons.add),
          backgroundColor: Colors.indigo,
        ),
      body: conversations.length > 0
      ? ListView.builder(
        itemCount: conversations.length,
        itemBuilder: (BuildContext context, int position) {
          return _buildCard(conversations[position]);
        },
      )
      : Center(
        child: Text("You have no chats yet.",
          style: TextStyle(
            fontSize: 14,
          ),
        ),
      ),
    );

  }
}