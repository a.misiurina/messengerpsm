class Conversation {
  int _id;
  int _userID;
  int _interlocutorID;
  bool _notificationsEnabled;

  Conversation (this._userID, this._interlocutorID);

  Conversation.fromMap(Map<String, dynamic> map) {
    this._id = map['IDDialog'];
    this._userID = map['IDUser'];
    this._interlocutorID = map['IDInterlocutor'];
    this._notificationsEnabled = (map['NotificationsEnabled'] == 1);
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['IDDialog'] = this._id;
    map['IDUser'] = this._userID;
    map['IDInterlocutor'] = this._interlocutorID;
    map['NotificationsEnabled'] = (this._notificationsEnabled == null) ? null : (this._notificationsEnabled ? 1 : 0);
    return map;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  int get userID => _userID;

  int get interlocutorID => _interlocutorID;

  // ignore: unnecessary_getters_setters
  bool get notificationsEnabled => _notificationsEnabled;

  // ignore: unnecessary_getters_setters
  set notificationsEnabled(bool value) {
    _notificationsEnabled = value;
  }

}