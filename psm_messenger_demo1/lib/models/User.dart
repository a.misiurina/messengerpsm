class User {
  int _id;
  String _username;
  String _firstName;
  String _lastName;
  String _email;
  String _phoneNumber;
  String _about;

  User(this._id, this._username, this._firstName, this._lastName, this._email, this._phoneNumber, this._about);
  User.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._username = map['username'];
    this._firstName = map['first_name'];
    this._lastName = map['last_name'];
    this._email = map['email'];
    this._phoneNumber = map['phone_number'];
    this._about = map['about'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = this._id;
    map['username'] = this._username;
    map['first_name'] = this._firstName;
    map['last_name'] = this._lastName;
    map['email'] = this._email;
    map['phone_number'] = this._phoneNumber;
    map['about'] = this._about;
    return map;
  }

  int get id => _id;

  String get username => _username;

  String get firstName => _firstName;

  String get lastName => _lastName;

  String get email => _email;

  String get phoneNumber => _phoneNumber;

  String get about => _about;

}