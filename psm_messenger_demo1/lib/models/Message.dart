class Message {
  int _id;
  int _dialogID;
  int _senderID;
  int _recipientID;
  int _sendingTime;
  String _messageContents;
  String _encryptionKey;
  bool _isSeen;

  Message (this._dialogID, this._senderID, this._recipientID, this._messageContents) :
    this._sendingTime = (new DateTime.now().millisecondsSinceEpoch) ~/ 1000,
    this._encryptionKey = "JX1YXcpNhuzCz1FqjlfmQnInMaQSMUTs";

  Message.fromMap(Map<String, dynamic> map) {
    this._id = map['IDMessage'];
    this._dialogID = map['IDDialog'];
    this._senderID = map['IDSender'];
    this._recipientID = map['IDRecipient'];
    this._sendingTime = map['SendingTime'];
    this._messageContents = map['MessageContents'];
    this._encryptionKey = map['EncryptionKey'];
    this._isSeen = (map['IsSeen'] == 1);
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['IDMessage'] = this._id;
    map['IDDialog'] = this._dialogID;
    map['IDSender'] = this._senderID;
    map['IDRecipient'] = this._recipientID;
    map['SendingTime'] = this._sendingTime;
    map['MessageContents'] = this._messageContents;
    map['EncryptionKey'] = this._encryptionKey;
    map['IsSeen'] = (this._isSeen == null) ? null : (this._isSeen ? 1 : 0);
    return map;
  }

  int get id => _id;

  int get dialogID => _dialogID;

  int get senderID => _senderID;

  int get recipientID => _recipientID;

  int get sendingTime => _sendingTime;

  String get text => _messageContents;

  String get encryptionKey => _encryptionKey;

  // ignore: unnecessary_getters_setters
  bool get isSeen => _isSeen;

  // ignore: unnecessary_getters_setters
  set isSeen(bool value) {
    _isSeen = value;
  }

}