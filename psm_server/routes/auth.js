const express = require('express');
const router = express.Router();
const Joi = require('joi');
const crypto = require('crypto');
const config = require('config');
const jwt = require('jsonwebtoken');
const mysql = require('mysql');

const db = mysql.createConnection(config.get('mysqldb'));

function validatePostBody(body) {
    const schema = {
        "Username": Joi.string(),
        "Password": Joi.string().min(8)
    }

    const validation = Joi.validate(body, schema);

    if (validation.error) {
        return validation.error.details[0].message;
    }
}

async function validateCredentials(username, password) {
    const p = new Promise((resolve, reject) => {
        // SELECT Salt FROM User
        const q1 = `SELECT COUNT(*) \`Count\`, IDUser, Salt FROM \`User\` WHERE (Username = ${db.escape(username)});`;

        db.query(q1, function (err, results, fields) {
            if (err) {
                reject(new Error(`Couldn't get salt for user @${username}:\n` + err.stack));
                return;
            }

            if (results[0].Count == 0) {
                resolve(false);
            } else {
                const idUser = results[0].IDUser;
                const hash = crypto.createHash('sha256').update(password + results[0].Salt).digest('hex');

                // SELECT COUNT(*) FROM User
                const q2 = `SELECT COUNT(*) \`Count\` FROM \`User\`
                    WHERE (IDUser = ${db.escape(idUser)}) AND (Password = ${db.escape(hash)});`;

                db.query(q2, function (err, results, fields) {
                    if (err) {
                        reject(new Error(`Couldn't get salt for user @${username}:\n` + err.stack));
                        return;
                    }

                    if (results[0].Count == 0) {
                        resolve(false);
                    } else {
                        resolve(idUser);
                    }
                });
            }
        });
    });

    return await p.then(result => result);
}

router.post('/', async (req, res) => {
    console.log('Auth attempt from ' + req.ip);
    const validationError = validatePostBody(req.body);
    if (validationError) {
        res.status(400).send(validationError);
        return;
    }

    try {
        const idUser = await validateCredentials(req.body.Username, req.body.Password);
        if (idUser) {
            const authToken = jwt.sign({ _id: idUser }, config.get('jwtPrivateKey'));
            res.header('x-auth-token', authToken).send(`${idUser}`);
        } else {
            res.status(400).send(`Wrong username or password.`);
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).send(`Internal Server Error`);
    }
});

module.exports = router;