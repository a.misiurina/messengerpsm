const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const config = require('config');
const mysql = require('mysql');

const db = mysql.createConnection(config.get('mysqldb'));

async function getUserFromDb(idUser) {
    const p = new Promise((resolve, reject) => {
        const q1 = `SELECT COUNT(*) Count, FirstName, LastName, Username, Email, PhoneNumber, About
            FROM User INNER JOIN UserProfile
            ON User.IDUser = UserProfile.IDUser 
            WHERE User.IDUser = ${db.escape(idUser)};`;

        db.query(q1, function (err, results, fields) {
            if (err) {
                reject(new Error(`Couldn't get user from database.`));
                return;
            }

            if (results[0].Count != 0) {
                user = {
                    "FirstName": results[0].FirstName,
                    "LastName": results[0].LastName,
                    "Username": results[0].Username,
                    "Email": results[0].Email,
                    "PhoneNumber": results[0].PhoneNumber,
                    "About": results[0].About
                }
                resolve(user);
            }
        });
    });

    return await p.then(result => result);
}

router.get('/:id', auth, async (req, res) => {
    try {
        const user = await getUserFromDb(req.params.id);
        if (user != {}) {
            res.json(user);
        } else {
            res.status(404).send(`User with id ${req.params.id} does not exist.`);
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).send(`Internal Server Error`);
    }
});

module.exports = router;