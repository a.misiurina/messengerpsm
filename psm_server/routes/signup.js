const express = require('express');
const router = express.Router();
const Joi = require('joi');
const crypto = require('crypto');
const config = require('config');
const jwt = require('jsonwebtoken');
const mysql = require('mysql');

const db = mysql.createConnection(config.get('mysqldb'));

function validatePostBody(body) {
    const schema = {
        "FirstName": Joi.string(),
        "LastName": Joi.string(),
        //"Email": Joi.string().email(),
        //"PhoneNumber": Joi.string(),
        "Username": Joi.string(),
        "Password": Joi.string().min(8)
    }

    const validation = Joi.validate(body, schema);

    if (validation.error) {
        return validation.error.details[0].message;
    }
}

async function isUsernameTaken(username) {
    const p = new Promise((resolve, reject) => {
        const q1 = `SELECT COUNT(*) \`Count\` FROM \`User\` WHERE (Username = ${db.escape(username)});`;

        db.query(q1, function (err, results, fields) {
            if (err) {
                reject(new Error(`Couldn't check username availability.`));
                return;
            }

            if (results[0].Count != 0) {
                resolve(true);
            } else {
                resolve(false);
            }
        });
    });

    return await p.then(result => result);
}

async function addUserToDb(user) {
    const p = new Promise((resolve, reject) => {
        let idUser;

        db.beginTransaction(function (err) {
            if (err) {
                reject(new Error('signup.js: Error in ADDUSER transaction: \n' + err.stack));
                return;
            }

            // INSERT INTO User
            const q1 = `INSERT INTO \`User\` (Username, Password, Salt)
                VALUES (${db.escape(user.Username)}, ${db.escape(user.Password)},
                ${db.escape(crypto.randomBytes(32).toString('hex'))});`;

            db.query(q1, function (err, results, fields) {
                if (err) {
                    reject(new Error('signup.js: Error in INSERT INTO User:\n' + err.stack));
                    return;
                }

                idUser = results.insertId;

                // UPDATE UserProfile
                const q2 = `UPDATE UserProfile SET FirstName = ${db.escape(user.FirstName)},
                    LastName = ${db.escape(user.LastName)} WHERE (IDUser = ${db.escape(idUser)})`;

                db.query(q2, function (err, results, fields) {
                    if (err) {
                        reject(new Error('signup.js: Error in UPDATE UserProfile:\n' + err.stack));
                        return;
                    }

                    // COMMIT TRANSACTION
                    db.commit(function (err) {
                        if (err) {
                            return db.rollback(function () {
                                reject(new Error('signup.js: Transaction ADDUSER rolled back:\n' + err.stack));
                            });
                        }

                        console.log(`signup.js: New user @${user.Username} has been added.`);
                        resolve(idUser);
                    });
                });
            });
        });
    });
    
    return await p.then(result => result);
}

router.post('/', async (req, res) => {
    const validationError = validatePostBody(req.body);
    if (validationError) {
        res.status(400).send(validationError);
        return;
    }

    try {
        const usernameTaken = await isUsernameTaken(req.body.Username);
        if (usernameTaken) {
            res.status(400).send(`Username @${req.body.Username} is already taken.`);
        } else {
            const idUser = await addUserToDb(req.body);
            if (idUser) {
                const authToken = jwt.sign({ _id: idUser }, config.get('jwtPrivateKey'));
                res.header('x-auth-token', authToken).send(`${idUser}`);
            } else {
                res.status(500).send(`Internal Server Error`);
            }
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).send(`Internal Server Error`);
    }
});

module.exports = router;