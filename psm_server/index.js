const Joi = require('joi');
const express = require('express');
const app = express();
app.use(express.json());
const cors = require("cors");
app.use(cors());
const crypto = require('crypto');
const config = require('config');
var mysql = require('mysql');

const signup = require('./routes/signup');
const auth = require('./routes/auth');
const profile = require('./routes/profile');
app.use('/psm/api/signup', signup);
app.use('/psm/api/auth', auth);
app.use('/psm/api/profile', profile);

if (!config.get('mysqldb.password') || !config.get('jwtPrivateKey')) {
    console.error('FATAL ERROR: Environment Variables are not configured.')
    process.exit(1);
}


const port = process.env.PORT || 3001;
app.listen(port, () => console.log(`Listening on port ${port}...`));